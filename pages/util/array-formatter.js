import React, { Component } from "react";
import utilStyles from '../../styles/utils.module.css'
import Layout from "../../components/layout";
import Checkbox from "../../components/checkbox";

class ArrayFormatter extends Component {

    constructor(props){
      super(props);
      this.state = {
        formattedJson: null,
        splitOnSpaces: false
      }
    }

    handleTextAreaInput = event => {
        const lines = event.target.value.split(/\r\n|\n\r|\n|\r/);
        let arrayText = '[\r\n'

        lines.forEach(element => {
            if(this.state.splitOnSpaces) {
                element.split(/\s+/).forEach(element2 => {
                    arrayText += '\t"' + element2.trim() + '",\r\n'
                })
            } else {
                arrayText += '\t"' + element.trim() + '",\r\n'
            }
        })

        arrayText = arrayText.slice(0, -3) + '\r\n'
        arrayText += "]"

        this.setState({
            formattedJson: arrayText
        })
    }

    handleSplitOnSpaces = changeEvent => {
        this.setState({
            splitOnSpaces: !this.state.splitOnSpaces
        })

        console.log(this.state.splitOnSpaces)
    }

    render() {
        return (
            <Layout>
                <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
                    <h1>Array Formatter</h1>
                </section>

                <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
                    <textarea onChange={this.handleTextAreaInput}></textarea>
                    <Checkbox label="Split on spaces" onCheckboxChange={this.handleSplitOnSpaces} />
                </section>

                <hr/>
                
                <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
                    <pre>{this.state.formattedJson}</pre>
                </section>
            </Layout>
        )
    }
}

export default ArrayFormatter

