import Head from 'next/head'
import Layout, { siteTitle } from '../components/layout'
import {getGeneratorsData} from '../lib/generators.js'
import GeneratorsList from '../components/generatorsList'

export async function getStaticProps() {
  const allGeneratorsData = getGeneratorsData()

  const generatorNames = allGeneratorsData.generators.map(({ name }) => {
    return name
  })

  return {
    props: {
      allGeneratorsData,
      generatorNames
    }
  }
}

export default function Home({allGeneratorsData, generatorNames}) {

  return (
    <Layout home>

      <Head>
        <title>{siteTitle}</title>
      </Head>

      <GeneratorsList allGeneratorsData={allGeneratorsData} generatorNames={generatorNames}></GeneratorsList>
    </Layout>
  )
}