import React, { Component } from "react"
import { useEffect, useRef } from "react";
import styles from "./scatterMapGenerator.module.css"

class ScatterMapGenerator extends Component {

  layer0 = null
  shadowLayer = null
  layer1 = null
  gridLayer = null
  layer2 = null
  layer3 = null

  mw = 10
  mh = 10

  app = null

  constructor(props) {
    super(props);
    this.state = {
      mw: 10,
      mh: 10
    }
  }


  componentDidMount() {
    this.app = new PIXI.Application({
      width: this.mw * 72, height: this.mh * 72, backgroundColor: 0x91ac4f, resolution: window.devicePixelRatio || 1,
    });

    this.renderMap()
    
  }

  componentWillUnmount() {
    console.log("Unmounting Pixi")
    this.destroyPixiContainers()
  }


  layers = [
    [
      "flowers-1",
      "rocks-1",
      "rocks-2",
      "stump-1",
      "random-shadow-1",
      "random-shadow-2",
      "random-shadow-3"
    ],
    [
      "tree-1",
      "tree-2",
      "tree-3",
      "tree-2",
      "tree-3",
      "tree-2",
      "tree-3",
      "tree-2",
      "tree-3",
      "tree-2",
      "tree-3",
      "tree-2",
      "tree-3",
      "tree-2",
      "tree-3",
      "tree-2",
      "tree-3",
      "tree-4",
      "tree-5"
    ]
  ]

  // layers = [
  //   [
  //     "Stone 1.png",
  //     "Stone 2.png",
  //     "Stone 3.png",
  //     "Stone 1.png",
  //     "Stone 2.png",
  //     "Stone 3.png",
  //     "Tree Stump 3 - Tree.png",
  //     "Tree Stump 1 - Tree.png",
  //     "Foliage 5 - Green 1.png",
  //     "Foliage 6 - Green 1.png",
  //     "Foliage 7 - Green 1.png",
  //     "Foliage 8 - Green 1.png",
  //     "Foliage 9 - Green 1.png",
  //     "Foliage 10 - Green 1.png",
  //     "Flowers 1.png",
  //     "Flowers 2.png",
  //     "Flowers 3.png",
  //     "Flowers 4.png",
  //     "Fallen Tree 2.png"
  //   ],
  //   [
  //     "Bush 1 - Red.png",
  //     "Bush 9 - Yellow.png",
  //     "Bush 2 - Green 1.png",
  //     "Bush 3 - Green 3.png",
  //     "Bush 3 - Green 3.png",
  //     "Bush 3 - Green 3.png",
  //     "Bush 3 - Green 3.png",
  //     "Bush 3 - Green 3.png",
  //     "Bush 3 - Green 3.png",
  //     "Bush 3 - Green 3.png",
  //     "Bush 3 - Green 3.png",
  //   ]
  // ]

  destroyPixiContainers() {
    if (this.layer0 == null) return

    this.layer0.destroy({
      children: true,
      texture: true,
      baseTexture: true
    })
    this.layer0 = null

    this.layer1.destroy({
      children: true,
      texture: true,
      baseTexture: true
    })
    this.layer1 = null

    this.shadowLayer.destroy({
      children: true,
      texture: true,
      baseTexture: true
    })
    this.shadowLayer = null

    this.layer2.destroy({
      children: true,
      texture: true,
      baseTexture: true
    })
    this.layer2 = null

    this.layer3.destroy({
      children: true,
      texture: true,
      baseTexture: true
    })
    this.layer3 = null

    this.gridLayer.destroy({
      children: true,
      texture: true,
      baseTexture: true
    })
    this.gridLayer = null

    document.getElementById('pixiContainer').removeChild(this.app.view);
    // this.app.destroy(true)
    // this.app = null
  }

  initializePixiContainers() {
    this.mw = 10 + Math.floor(Math.random() * 30)
    this.mh = 10 + Math.floor(Math.random() * 30)

    this.setState({
      mw: this.mw,
      mh: this.mh
    })

    this.app.renderer.resize(this.mw * 72, this.mh * 72)
    document.getElementById('pixiContainer').appendChild(this.app.view);

    this.layer0 = new PIXI.Container();
    this.app.stage.addChild(this.layer0);

    this.drawGrid()

    this.layer1 = new PIXI.Container();
    this.app.stage.addChild(this.layer1);

    this.shadowLayer = new PIXI.Container();
    this.app.stage.addChild(this.shadowLayer);

    this.layer2 = new PIXI.Container();
    this.app.stage.addChild(this.layer2);

    this.layer3 = new PIXI.Container();
    this.app.stage.addChild(this.layer3);

  }

  renderMap() {
    this.destroyPixiContainers()
    this.initializePixiContainers()

    const floorTex = PIXI.Texture.from('/map-assets/green-forest/' + encodeURI('background-1.png'));
    let wPixels = 407
    let hPixels = 407
    let wTiles = wPixels/72
    let hTiles = hPixels/72
    for (let x = 0; x < this.mw/wTiles; x++) {
      for (let y = 0; y < this.mh/hTiles; y++) {
        const floorTile = new PIXI.Sprite(floorTex)
        floorTile.x = x * wPixels
        floorTile.y = y * hPixels
        this.layer0.addChild(floorTile)
      }
    }

    /* create a connected map where the player can reach all non-wall sections */
    var map = new ROT.Map.Cellular(this.mw, this.mh, { connected: true });

    /* cells with 1/2 probability */
    map.randomize(0.57);

    /* make a few generations */
    for (var i = 0; i < 4; i++) map.create();
    // /* now connect the maze */
    var display = new ROT.Display({ width: this.mw, height: this.mh, fontSize: 4 });
    map.connect(display.DEBUG, 1);

    for (let x = 0; x < map._width; x++) {
      for (let y = 0; y < map._height; y++) {

        if (map._map[x][y] == 0) { // Trees

          const randIdx = Math.random() * (this.layers[1].length - 1);
          const fname = this.layers[1][Math.round(randIdx)];

          // const fname = encodeURI(fname)
          const texture = PIXI.Texture.from('/map-assets/green-forest/' + encodeURI(fname) + '.png');
          const sprite = new PIXI.Sprite(texture);
          sprite.anchor.set(0.5);

          sprite.x = x * 72 + 72 / 2;
          sprite.y = y * 72 + 72 / 2;
          sprite.angle = Math.random() * 360;
          sprite.scale.x = sprite.scale.y = 1 + Math.random() * 0.5;

          if(Math.random()*100 < 50) {
            this.layer2.addChild(sprite);
          } else {
            this.layer3.addChild(sprite);
          }

          // Add a shadow
          const shadowTex = PIXI.Texture.from('/map-assets/green-forest/' + encodeURI(fname) + '-shadow.png');
          const shadow = new PIXI.Sprite(shadowTex);
          shadow.anchor.set(0.5);
          shadow.angle = sprite.angle;
          shadow.x = sprite.x;
          shadow.y = sprite.y;
          shadow.scale.x = shadow.scale.y = sprite.scale.x;
          this.shadowLayer.addChild(shadow);


        } else if (Math.random() * 100 < 2) { // Random tree in an open space

          const randIdx = Math.random() * (this.layers[1].length - 1);
          const fname = this.layers[1][Math.round(randIdx)];

          // const fname = encodeURI(fname)
          const texture = PIXI.Texture.from('/map-assets/green-forest/' + encodeURI(fname) + '.png');
          const sprite = new PIXI.Sprite(texture);
          sprite.anchor.set(0.5);

          sprite.x = x * 72 + 72 / 2;
          sprite.y = y * 72 + 72 / 2;
          sprite.angle = Math.random() * 360;
          sprite.scale.x = sprite.scale.y = 1 + Math.random() * 1;
          this.layer2.addChild(sprite);

          // Add a shadow
          const shadowTex = PIXI.Texture.from('/map-assets/green-forest/' + encodeURI(fname) + '-shadow.png');
          const shadow = new PIXI.Sprite(shadowTex);
          shadow.anchor.set(0.5);
          shadow.angle = sprite.angle;
          shadow.x = sprite.x;
          shadow.y = sprite.y;
          shadow.scale.x = shadow.scale.y = sprite.scale.x;
          this.shadowLayer.addChild(shadow);

        } else if (Math.random() * 100 < 6) { // Random ground object

          const randIdx = Math.random() * (this.layers[0].length - 1);
          const fname = this.layers[0][Math.round(randIdx)];

          // const fname = encodeURI(fname)
          const texture = PIXI.Texture.from('/map-assets/green-forest/' + encodeURI(fname) + '.png');
          const sprite = new PIXI.Sprite(texture);
          sprite.anchor.set(0.5);

          sprite.x = x * 72 + Math.floor(Math.random()*144) / 2;
          sprite.y = y * 72 + Math.floor(Math.random()*144) / 2;
          sprite.angle = Math.random() * 360;
          sprite.scale.x = sprite.scale.y = 1 + Math.random() * 0.5;
          this.layer1.addChild(sprite);
        }
      }
    }

  }

  drawGrid() {
    this.gridLayer = new PIXI.Graphics();
    this.gridLayer.lineStyle(2, 0x748642, 1);

    for (let y = 0; y < this.app.screen.height; y += 72) {
      this.gridLayer.moveTo(0, y);
      this.gridLayer.lineTo(this.app.screen.width, y)
    }
    for (let x = 0; x < this.app.screen.width; x += 72) {
      this.gridLayer.moveTo(x, 0);
      this.gridLayer.lineTo(x, this.app.screen.height)
    }

    this.app.stage.addChild(this.gridLayer)
  }

  downloadImage() {
    let renderTexture = PIXI.RenderTexture.create(this.mw*72, this.mh*72);
    this.app.renderer.render(this.app.stage,renderTexture)

    this.app.renderer.extract.canvas(renderTexture).toBlob((b) => {
        const a = document.createElement('a');
        document.body.append(a);
        a.download = 'forest-map-'+this.mw+"x"+this.mh;
        a.href = URL.createObjectURL(b);
        a.click();
        a.remove();
    }, 'image/jpeg', 0.8);
  }



  render() {
    return (
      <>
        <section className={styles.titleContainer}>
        <p className={styles.mapSizeSpan}>
            Forest Encounter Map - {this.state.mw} x {this.state.mh}
          </p>
          <button className={styles.regenerateButton} onClick={() => { this.renderMap() }}>Randomize</button>
          <button className={styles.downloadButton} onClick={() => { this.downloadImage() }}>Download Image</button>


          {/* <input value={this.mw}></input>
      <input value={this.mh}></input> */}
        </section>
        <div id="pixiContainer"></div>
      </>
    );
  }
}

export default ScatterMapGenerator