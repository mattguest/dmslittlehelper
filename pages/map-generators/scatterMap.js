import Head from 'next/head'
import Layout from '../../components/layout'
import dynamic from 'next/dynamic'

const ScatterMapGenerator = dynamic(() => import("./scatterMapGenerator"), {
    ssr: false,
  }); 

export default function ScatterMap() {

      return (
        <Layout>
            <Head>
                <title>Map Generator</title>
                <script src="/pixi.js"></script>
                <script src="/rot.js"></script>
            </Head>
            <ScatterMapGenerator mw="20" mh="20"></ScatterMapGenerator>
        </Layout>
    )
}
