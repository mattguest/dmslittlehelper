import fs from 'fs'
import path from 'path'
const listsDirectory = path.join(process.cwd(), 'data')

export default function userHandler(req, res) {
    const {
      query: { id, data },
      method,
    } = req
  
    switch (method) {
      case 'GET':
        // read and return the contents of the json file
        const fullPath = path.join(listsDirectory, id+".json")
        const fileContents = fs.readFileSync(fullPath, 'utf8')
        const jsonData = JSON.parse(fileContents)
        res.status(200).json(jsonData)
        break


    case 'PUT':
        // Update or create data in your database
        res.status(501)
        break


    default:
        res.setHeader('Allow', ['GET', 'PUT'])
        res.status(405).end(`Method ${method} Not Allowed`)
    }
}

