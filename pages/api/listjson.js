import fs from 'fs'
import path from 'path'

const listsDirectory = path.join(process.cwd(), 'data/lists')

export default (req, res) => {
    const fullPath = path.join(listsDirectory, "beholders.json")
    const fileContents = fs.readFileSync(fullPath, 'utf8')

    const jsonData = JSON.parse(fileContents)
    res.statusCode = 200
    res.json(jsonData)
}
  