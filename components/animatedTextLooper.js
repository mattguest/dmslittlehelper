import React, { Component } from "react"
import styles from './animatedTextLooper.module.css'


class AnimatedTextLooper extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            placeholderText: "",
            period: 1200,
            isDeleting: false,
            fullText: ""
        };
    }

    componentDidMount() {
        this.pickRandomString()
        this.doTick()
    }

    pickRandomString() {
        const randIdx = Math.random() * (this.props.strings.length - 1)
        const picked = this.props.strings[Math.round(randIdx)]

        this.setState({
            fullText: picked
        })
    }

    doTick() {
        const fText = this.state.fullText
        let pText = this.state.placeholderText
        let isDeleting = this.state.isDeleting

        if (isDeleting) {
            pText = fText.substring(0, pText.length - 1)
        } else {
            pText = fText.substring(0, pText.length + 1)
        }

        let delta = this.state.period / fText.length
        if(isDeleting) { delta /= 2 }

        if(!isDeleting && pText === fText) {
            delta = this.state.period;
            isDeleting = true;
        } else if (isDeleting && pText === '') {
            isDeleting = false
            this.pickRandomString()
            delta = this.state.period/2
        }

        this.setState({
            placeholderText: pText,
            isDeleting: isDeleting,
        })

        let that = this;
        setTimeout(() => {
            that.doTick()
        }, delta)
    }

    handleInput = event => {
        this.props.onChange(event.target.value)
    }

    render() {
        return (
            <input 
                className={styles.textInput}
                placeholder={this.state.placeholderText} 
                onChange={this.handleInput}
            ></input>
        );
    }
}

export default AnimatedTextLooper
