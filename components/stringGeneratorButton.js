import React, { Component } from "react";
import utilStyles from '../styles/utils.module.css'
import styles from './generatorButton.module.css'
import axios from 'axios'
import buildRandomString from '../lib/randomStringBuilder.js'

const fetcher = url => fetch(url).then(res => res.json());

class StringGeneratorButton extends Component {
    
    constructor() {
        super();
        this.state = {
            generatorName: "",
            generatorData: null,
            itemPicked: "",
            contentDisplayed: false,
            pickedBuilder: "",
            hiding: false,
            bgColor: ''//"hsl(" + 360 * Math.random() + ',' + (25 + 70 * Math.random()) + '%,' + (85 + 10 * Math.random()) + '%)'
        };
    }

    componentDidMount() {
        this.setState({
            generatorName: this.props.generatorName
        })
    }


    dec2hex (dec) {
        return dec.toString(16).padStart(2, "0")
    }
    doRandomTextSpinner() {
        let newText = ""
        var arr = new Uint8Array(this.props.generatorName.length / 2)
        window.crypto.getRandomValues(arr)
        newText = Array.from(arr, this.dec2hex).join('')

        this.setState({
            generatorName: newText
        })
    }

    async pickRandomItem() {
        this.setState({
            hiding: true
        })

        const spinnerId = setInterval(() => {
            this.doRandomTextSpinner()
        }, 1)

        let randomString = await buildRandomString(this.props.generatorId)

        setTimeout(() => {
            setTimeout(() => {
                clearInterval(spinnerId)
                this.setState({
                    itemPicked: randomString,
                    contentDisplayed: true,
                    hiding: false,
                    generatorName: this.props.generatorName
                })
              }, 200)
        }, 50)
    }

    render() {

        return (
            <li 
                key={this.props.generatorId} 
                className={utilStyles.generatorItem}>
                <button 
                    style={this.state.contentDisplayed ? {
                        backgroundColor: this.props.color
                    } : {}}
                    // className={`${styles.generatorButton} ${this.state.contentDisplayed ? styles.gbContentDisplayed : styles.gbNoContent}`}
                    className={`${styles.gbutton} ${this.state.contentDisplayed ? styles.gbuttoncontent : ""}`}
                    onClick={ () => this.pickRandomItem() }>
                    {this.state.generatorName}
                </button>

                <p 
                    style={{
                        backgroundColor: this.props.color,
                        display: this.state.contentDisplayed ? "block":"none"
                    }}
                    className={`${styles.pickedText} ${this.state.hiding ? styles.hiding : null}`}
                    dangerouslySetInnerHTML={{__html: this.state.itemPicked}}
                >
                </p>
            </li>
        );
    }
}

export default StringGeneratorButton
