import React, { Component } from "react";
import utilStyles from '../styles/utils.module.css'
import StringGeneratorButton from './stringGeneratorButton.js'
import AnimatedTextLooper from './animatedTextLooper.js'
import MapGeneratorButton from "./mapGeneratorButton";


class GeneratorsList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchText: ""
        }
    }

    onSearchChange(searchText) {
        console.log("search: ", searchText)
        this.setState({
            searchText: searchText
        })
    }

    getGroupByName(name) {
        const result = this.props.allGeneratorsData.groups.find( ({ groupName }) => groupName === name );
        return result
    }

    render() {

        let generatorList = []
        console.log("allGeneratorsData: ", this.props.allGeneratorsData)
        
        const allGeneratorsData = this.props.allGeneratorsData.generators

        let currentGroup = null;

        for(let i=0; i<allGeneratorsData.length; i++) {
            const li = allGeneratorsData[i];
            const generatorName = li.filterName.toLowerCase().trim()
            const st = this.state.searchText.toLowerCase().trim()
            if(st.length < 1 || generatorName.indexOf(st) > -1) {
                if(currentGroup==null || li.groupName !== currentGroup.groupName) {
                    currentGroup = this.getGroupByName(li.groupName);
                    generatorList.push(
                        <li key={`${li.id}-group`}>
                            <h3
                                style={{backgroundColor: currentGroup.color}}>
                                {currentGroup.groupName}
                            </h3>
                        </li>
                    )
                }

                if(li.type == "stringbuilder") {
                    generatorList.push(
                        <StringGeneratorButton 
                            generatorId={li.id} 
                            key={li.id} 
                            generatorName={li.name} 
                            items={li.items}
                            color={currentGroup.color}>
                        </StringGeneratorButton>
                    )
                }

                if(li.type == "mapgenerator") {
                    generatorList.push(
                        <MapGeneratorButton
                            generatorId={li.id}
                            key={li.id}
                            generatorName={li.name}
                            items="[]"
                            color={li.color}>
                        </MapGeneratorButton>
                    )
                }
            }
        }

        return (
            <>
            <header className={utilStyles.header}>
            <h1 className={utilStyles.heading2Xl}>
                "Quick! DM's Little Helper, I need a {" "}
                <AnimatedTextLooper 
                    strings={this.props.generatorNames}
                    onChange={(searchText) => this.onSearchChange(searchText)}
                ></AnimatedTextLooper>!"
            </h1>
            </header>

            <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
            <ul className={utilStyles.list}>
                {generatorList}
            </ul>
            </section>
          </>
        );
    }
}

export default GeneratorsList
