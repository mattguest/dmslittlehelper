import React, { Component } from "react";
import utilStyles from '../styles/utils.module.css'
import styles from './generatorButton.module.css'
import { useRouter } from 'next/router'

function MapGeneratorButton(props) {
    const router = useRouter()
  
    const goToMapGenerator = (e) => {
      e.preventDefault()
      console.log(window);
      window.location = '/map-generators/scatterMap'
    }
  
    return (
        <li 
            key={props.generatorId} 
            className={utilStyles.generatorItem}>
            <button 
                className={`${styles.gbutton}`}
                onClick={goToMapGenerator}>
                {props.generatorName}
            </button>
        </li>
    )
  }
  
  export default MapGeneratorButton
