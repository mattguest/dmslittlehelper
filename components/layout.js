import Head from 'next/head'
import Image from 'next/image'
import styles from './layout.module.css'
import utilStyles from '../styles/utils.module.css'
import Link from 'next/link'

const name = 'DM\'s Little Helper!'
export const siteTitle = 'DM\'s Little Helper!'

export default function Layout({ children, home }) {
  return (
    <div className={styles.container}>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="Quick and easy random generators for dungeon masters."
        />
        <meta
          property="og:image"
          content={`https://og-image.vercel.app/${encodeURI(
            siteTitle
          )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
        />
        <meta name="og:title" content={siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>

      <div className={styles.topbar}>
      {!home && (
          <Link href="/">
            <a className={styles.homeLink} >← Generator List</a>
          </Link>
      )}
      {home && (
          <p style={
            {
              textAlign: "center",
              lineHeight: "1em",
              margin: 0
            }
          } className={styles.homeLink} >DM's Little Helper <br/><span className={styles.tagline}>(still totally not spam!)</span></p>
      )}
      </div>
      <main>{children}</main>
    </div>
  )
}