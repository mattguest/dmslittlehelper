The random generator uses json files to construct a string from randomly selected elements in arrays

The basic format is:

```
{
    "listName": "Character Names",
    "items": [
        [
            "Billy",
            "Bobby",
            "Brady",
            "Bubbilicious"
        ],
        [
            "Bader",
            "Bungalo",
            "Bluejeans",
            "Barrimore"
        ]
    ]
}

///// Explanation ////

{
    // This is used to display the text on the generate button
    "listName": "Character Names",

    // The items array that contains all the parts of the string
    // The picker will iterate through all of the arrays within it 
    // and pick one item from each to concat to the string.
    "items": [

        // Items to choose from for the first part of the string
        [
            "Billy",
            "Bobby",
            "Brady",
            "Bubbilicious"
        ],

        // Items to choose from for the second part of the array
        [
            "Bader",
            "Bungalo",
            "Bluejeans",
            "Barrimore"
        ]
    ]
}
```

Any of the arrays in the items array can also be defined as a 'complex' object instead of a string.
This allows additional items to be concated only if that object is picked, and the ability to change the odds that the item will be picked.

```
{
    "listName": "Character Names",
    "items": [
        [
            "normal string item",
            {
                "text": "complex object",
                "multiply": 5,
                "items": [
                    [
                        "sub item 1",
                        "sub item 2",
                        "sub item 3"
                    ]
                ]
            }
        ]
    ]
}

///// Explanation ////

{
    "listName": "Character Names",
    "items": [
        [
            // A normal string item, like in the first example
            "normal string item",

            // a complex object
            {
                // the text to concat for this item
                "text": "complex object",

                // the number of times this object should be multiplied to 
                // increase the odds of it being picked. In this case it 
                // will be multiplied, or duplicated, 5 times.
                "multiply": 5,

                // a branched items array that works just as the basic ones do, 
                // but only when this item was picked. It can also contain complex objects
                "items": [
                    [
                        "sub item 1",
                        "sub item 2",
                        "sub item 3"
                    ]
                ]
            }
        ]
    ]
}
```