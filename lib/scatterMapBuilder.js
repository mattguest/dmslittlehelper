import axios from 'axios'

let generatorsData = {}

async function loadGeneratorData(stringBuilderId) {
    console.log("scatterMapBuilder ", "loadGeneratorData", stringBuilderId)

    if(generatorsData.hasOwnProperty(stringBuilderId)) {
        return generatorsData[stringBuilderId]
    }

    const response = await axios.get(
        '/api/jsondata/'+stringBuilderId
    );

    generatorsData[stringBuilderId] = response.data

    console.log("scatterMapBuilder ", "loadGeneratorData finished", generatorsData, stringBuilderId)

    return generatorsData[stringBuilderId]
}

async function generateMap(generatorId) {
    return generatorsData[generatorId]
}

export default async function generateScatterMap(generatorId, options={}) {
    let generatorData = await loadGeneratorData(generatorId)
    console.log("GENERATOR DATA:", generatorData.items)
    let map = await generateMap(generatorData)

    return map;
}
