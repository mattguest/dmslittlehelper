import axios from 'axios'

let generatorsData = {}

async function loadGeneratorData(stringBuilderId) {
    // console.log("buildRandomString ", "loadGeneratorData", stringBuilderId)

    if(generatorsData.hasOwnProperty(stringBuilderId)) {
        return generatorsData[stringBuilderId]
    }

    const response = await axios.get(
        '/api/jsondata/'+stringBuilderId
    );

    generatorsData[stringBuilderId] = response.data

    console.log("buildRandomString ", "loadGeneratorData finished", generatorsData, stringBuilderId)

    return generatorsData[stringBuilderId]
}

function addToString(stringBuilder, newText, settings) {
    if(newText.length < 1) return stringBuilder;
    console.log("addToString: ", "'"+stringBuilder+"'", ", ", "'"+newText+"'")
    newText = settings.preAppendEach + newText + settings.postAppendEach
    let str = stringBuilder.trim() + settings.separator + newText.trim() + settings.separator
    console.log("addToString: newstring: ", "'"+str+"'")
    return str
}

function fixStringFormatting(strToFix) {
    let str = strToFix.trim()

    // multitple spaces
    str = str.replaceAll('  ', ' ')
    str = str.replaceAll('   ', ' ')
    str = str.replaceAll('    ', ' ')

    // space removers
    str = str.replaceAll('[<] ', '')
    str = str.replaceAll('[<]', '')
    str = str.replaceAll(' [>]', '')
    str = str.replaceAll('[>]', '')

    // fix a/an
    str = str.replaceAll('a a', 'an a')
    str = str.replaceAll('a A', 'an A')
    str = str.replaceAll('a e', 'an e')
    str = str.replaceAll('a E', 'an E')
    str = str.replaceAll('a i', 'an i')
    str = str.replaceAll('a I', 'an I')
    str = str.replaceAll('a o', 'an o')
    str = str.replaceAll('a O', 'an O')
    str = str.replaceAll('a u', 'an u')
    str = str.replaceAll('a U', 'an U')

    // fix periods
    str = str.replaceAll(' .', '.')

    str = str.charAt(0).toUpperCase() + str.slice(1);

    return str
}

async function  pickRecursive(items, settings, pickedBuilder, stringBuilderId) {
     console.log("buildRandomString ", "pickRecursive", stringBuilderId, settings)

    // construct the string
    for(let i=0; i<items.length; i++) {

        // check for object items at this level and apply the 
        // 'multiply' option to modify the odds
        let itemsToPickFrom = []
        for(let i2=0; i2<items[i].length; i2++) {
            if(typeof(items[i][i2]) == 'object' && items[i][i2].hasOwnProperty('settings') && items[i][i2]['settings'].hasOwnProperty('multiply')) {
                for(let c=0; c<items[i][i2].settings.multiply; c++) {
                    itemsToPickFrom.push(items[i][i2])
                }
            } else {
                itemsToPickFrom.push(items[i][i2])
            }
        }

        const randIdx = Math.random() * (itemsToPickFrom.length - 1);
        const item = itemsToPickFrom[Math.round(randIdx)];
        if(typeof(item) == 'object') {
            let objSettings = mergeSettings(item.hasOwnProperty('settings')?item['settings']:{}, settings);
            console.log("Object item settings ", objSettings)
            let repeatCount = 1;
            if(objSettings.pickMin!==1 || objSettings.pickMax !== 1) {
                repeatCount = objSettings.pickMin + Math.round(Math.random() * (objSettings.pickMax-objSettings.pickMin))
                if(repeatCount == 0) {
                    pickedBuilder = addToString(pickedBuilder, objSettings.nonePickedText, settings)
                }
            }

            for(let repeater=1; repeater<=repeatCount; repeater++) {
                console.log("Hit object item: ", item)
                if(item.useGenerator && item.useGenerator.length>0) {
                    // console.log("Branching to a different generator: ", item.useGenerator)
                    let newText = await buildRandomString(item.useGenerator, true)
                    // if(item.hasOwnProperty("preAppendEach")) newText = item.preAppendEach + newText;
                    // if(item.hasOwnProperty("postAppendEach")) newText = newText + item.postAppendEach;
                    pickedBuilder = addToString(pickedBuilder, newText, objSettings)
                } else {
                    pickedBuilder = addToString(pickedBuilder, objSettings.text, objSettings)
                    if(item.hasOwnProperty("items")) pickedBuilder += await pickRecursive(item.items, objSettings, "", stringBuilderId)
                    if(objSettings.fixImmediately) pickedBuilder = fixStringFormatting(pickedBuilder)
                }
            }
        } else pickedBuilder = addToString(pickedBuilder, item, settings)
    }

    // console.log("buildRandomString ", "pickedbuilder", pickedBuilder, stringBuilderId)
    return pickedBuilder
}

function mergeSettings(newSettings, baseSettings={}) {
    if(newSettings == null) newSettings = {}

    let defaultSettings = {
        "fixImmediately": false,
        "separator": " ",
        "multiply": 1,
        "preAppendEach": '',
        "postAppendEach": '',
        "nonePickedText": '',
        "pickMin": 1,
        "pickMax": 1,
        "text": ''
    };

    let settings = {...defaultSettings, ...baseSettings}
    return {...settings, ...newSettings}
}

export default async function buildRandomString(stringBuilderId, calledFromRecursive=false) {    
    let generatorData = await loadGeneratorData(stringBuilderId)
    let settings = mergeSettings(generatorData.hasOwnProperty('settings')?generatorData.settings:{})
    let settings2 = mergeSettings(generatorData.hasOwnProperty('settings')?generatorData.settings:{})

    let picked = await pickRecursive(generatorData.items, mergeSettings(settings), "", stringBuilderId)
    
    if(!calledFromRecursive || settings.fixImmediately) picked = fixStringFormatting(picked)

    console.log("BRS return: ", picked, settings, settings2)

    return picked
}
