import fs from 'fs'
import path from 'path'

const generatorsDirectory = path.join(process.cwd(), 'data')

export function getGeneratorsData() {

  const fullPath = path.join(generatorsDirectory, '_generators.json')
  const fileContents = fs.readFileSync(fullPath, 'utf8')
  const jsonData = JSON.parse(fileContents)

  return jsonData
}
